FROM arm32v7/python:3.7.4-stretch
# docker build -t daitangio/rpi-basecamp .
# Basic updated image optimized BELOW 120MB using teh arm32v7/debian
LABEL Description="Flask Metrics for RasperryPI 2/3" Vendor="Giovanni Giorgi" Version="1.0"
# Python already here
#RUN apt-get update && \
#    apt-get -qy install curl ca-certificates
RUN  python -m pip install --upgrade pip==19.1.1
# TODOCopy stuff with ADD then...
WORKDIR /usr/src/app
ADD requirements.txt ./
RUN pip install  --no-cache-dir -r requirements.txt
ADD metron ./

